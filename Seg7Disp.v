module Seg7Disp(
	input clk50,
	input reset,
	output reg [2:0]rgb,
	output reg vsync,
	output reg hsync,
	output speaker,
	input inc_hr,
	input inc_min
);

reg clk; 

music bgms(.clk(clk), .speaker(speaker));

//RELOJ
reg [25:0] cnt = 26'd0;
//reg [5:0] segs;
reg clk1 = 1'b0;
reg [5:0] outm, outs;
reg [4:0] outh;

reg [9:0] hcount, vcount;
always @(posedge clk50)
begin
	if(reset)
	begin
		clk = 0;
	end
	else
		clk = !clk;
end

always @(posedge clk50)
begin
	if(cnt == 25000000)
	begin
		clk1 = !clk1;
		cnt = 0;
	end
	else
		cnt = cnt + 1;
end

always @(posedge clk1)
begin
	if(inc_hr == 1'b1)
		outh = outh + 6'd1;
	if(inc_min == 1'b1)
		outm = outm + 6'd1;
	if(reset)
	begin
		outh = 5'd0;
		outm = 6'd0;
		outs = 6'd0;
	end
	else
	begin
		if(outs != 6'd59)
			outs = outs + 6'd1;
		else if(outs == 6'd59)
		begin
			outs = 6'd0;
			outm = outm + 6'd1;
		end
		if(outm == 6'd59)
		begin
			outm = 6'd0;
			outh = outh + 6'd1;
		end
		if(outh == 6'd23)
			outh = 6'd0;
	end
end

//VGA
always @(posedge clk)
begin
if(reset)
	begin
		//segs = 6'd0;
		hcount = 0;
		vcount = 0;
	end
else

begin
if (hcount == 799)
begin
		hcount = 0;
		if (vcount == 524)
			begin
			//segs = segs + 6'd1;
			vcount = 0;
			end
	else
		vcount = vcount + 1'b1;
end

else
	hcount = hcount + 1'b1;

if (vcount >= 490 && vcount < 492)
	vsync = 0;
else
	vsync = 1;

if (hcount >= 656 && hcount < 752)
	hsync = 0;
else
	hsync = 1;

if (hcount < 640 && vcount < 480) // negro toda
begin
		if(hcount >= 90 && hcount < 100 && vcount >=30 && vcount < 40)
			rgb = 3'b001;
		else if(hcount >=100 && hcount < 105 && vcount >=30 && vcount < 40)
			rgb = 3'b000;
		
		if(hcount >= 90 && hcount < 100 && vcount >=50 && vcount < 60)
			rgb = 3'b001;
		else if(hcount >=100 && hcount < 105 && vcount >=50 && vcount < 60)
			rgb = 3'b000;
			
		/*if(hcount >= 175 && hcount < 185 && vcount >=30 && vcount < 40)
			rgb = 3'b001;
		else if(hcount >=185 && hcount < 190 && vcount >=30 && vcount < 40)
			rgb = 3'b000;

		if(hcount >= 175 && hcount < 185 && vcount >=50 && vcount < 60)
			rgb = 3'b001;
		else if(hcount >=185 && hcount < 190 && vcount >=50 && vcount < 60)
			rgb = 3'b000;*/
		//SEGS 01
		/*if(outs >= 0 && outs < 10)
			begin
			if(hcount >= 190 && hcount < 220 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			else if(hcount >= 190 && hcount < 200 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;
			else if(hcount >= 200 && hcount < 210 && vcount >= 30 && vcount < 60)
				rgb = 3'b000;
			else if(hcount >= 210 && hcount < 220 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;	
			else if(hcount >= 190 && hcount < 220 && vcount >= 60 && vcount < 70)
				rgb = 3'b001;
			else if(hcount >= 220 && hcount < 225 && vcount >= 20 && vcount < 70)
				rgb = 3'b000;
			end
		else if (outs >=10 && outs < 20)
			begin
			if(hcount >= 190 && hcount < 210 && vcount  >= 20 && vcount < 70)
				rgb = 3'b000;
			else if (hcount >= 210 && hcount < 220 && vcount  >= 20 && vcount < 70)
				rgb = 3'b001;
			else if (hcount >= 220 && hcount < 225 && vcount  >= 20 && vcount < 70)
				rgb = 3'b000;
			end
		else if(outs >=20 && outs < 30)
			begin
			if(hcount >= 190 && hcount < 220 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 210 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 210 && hcount < 220 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 220 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 200 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 200 && hcount < 220 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 190 && hcount < 220 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 220 && hcount < 225 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs >=30 && outs < 40)
			begin
			if(hcount >= 190 && hcount < 220 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 210 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 210 && hcount < 220 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 220 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 210 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 210 && hcount < 220 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 220 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 220 && hcount < 225 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs >=40 && outs < 50)
			begin
			if(hcount >= 190 && hcount < 200 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 200 && hcount < 210 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 210 && hcount < 220 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if (hcount >= 190 && hcount < 220 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;	
				else if(hcount >= 190 && hcount < 210 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;	
				else if(hcount >= 210 && hcount < 220 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 220 && hcount < 225 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if (outs >=50 && outs < 60)
			begin
			if(hcount >= 190 && hcount < 220 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 200 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 200 && hcount < 220 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 190 && hcount < 220 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 210 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 210 && hcount < 220 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 190 && hcount < 220 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 220 && hcount < 225 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		//SEGS 00
		if(outs == 6'd0 || outs == 6'd10 || outs == 6'd20 || outs == 6'd30 || outs == 6'd40 || outs == 6'd50)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			
				else if(hcount >= 225 && hcount < 235 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 245 && vcount >= 30 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;	
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		
		else if(outs == 6'd1 || outs == 6'd11 || outs == 6'd21 || outs == 6'd31 || outs == 6'd41 || outs == 6'd51)
			begin
				if(hcount >= 225 && hcount < 245 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
					else if (hcount >= 245 && hcount < 255 && vcount  >= 20 && vcount < 70)
					rgb = 3'b001;
					else if (hcount >= 255 && hcount < 640 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd2 || outs == 6'd12 || outs == 6'd22 || outs == 6'd32 || outs == 6'd42 || outs == 6'd52)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 235 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 255 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd3 || outs == 6'd13 || outs == 6'd23 || outs == 6'd33 || outs == 6'd43 || outs == 6'd53)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd4 || outs == 6'd14 || outs == 6'd24 || outs == 6'd34 || outs == 6'd44 || outs == 6'd54)
			begin
				if(hcount >= 225 && hcount < 235 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 245 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if (hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;	
				else if(hcount >= 225 && hcount < 245 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;	
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd5 || outs == 6'd15 || outs == 6'd25 || outs == 6'd35 || outs == 6'd45 || outs == 6'd55)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 235 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 255 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd6 || outs == 6'd16 || outs == 6'd26 || outs == 6'd36 || outs == 6'd46 || outs == 6'd56)
			begin
				if(hcount >= 225 && hcount < 235 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 255 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 235 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 245 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd7 || outs == 6'd17 || outs == 6'd27 || outs == 6'd37 || outs == 6'd47 || outs == 6'd57)
			begin
				if(hcount >= 225 && hcount < 255 && vcount > 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 30 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd8 || outs == 6'd18 || outs == 6'd28 || outs == 6'd38 || outs == 6'd48 || outs == 6'd58)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			
				else if(hcount >= 225 && hcount < 235 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 245 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;	
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 235 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 245 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outs == 6'd9 || outs == 6'd19 || outs == 6'd29 || outs == 6'd39 || outs == 6'd49 || outs == 6'd59)
			begin
				if(hcount >= 225 && hcount < 255 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 235 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 235 && hcount < 240 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 255 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 225 && hcount < 245 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 245 && hcount < 255 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 255 && hcount < 640 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end*/
		//MINS 01
		if(outm >=0 && outm <10)
			begin
			if(hcount >= 105 && hcount < 135 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			else if(hcount >= 105 && hcount < 115 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;
			else if(hcount >= 115 && hcount < 125 && vcount >= 30 && vcount < 60)
				rgb = 3'b000;
			else if(hcount >= 125 && hcount < 135 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;	
			else if(hcount >= 105 && hcount < 135 && vcount >= 60 && vcount < 70)
				rgb = 3'b001;
			else if(hcount >= 135 && hcount < 140 && vcount >= 20 && vcount < 70)
				rgb = 3'b000;
			end
		else if(outm >=10 && outm < 20)	
			begin
			if(hcount >= 105 && hcount < 125 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
				else if (hcount >= 125 && hcount < 135 && vcount  >= 20 && vcount < 70)
					rgb = 3'b001;
				else if (hcount >= 135 && hcount < 140 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm >=20 && outm < 30)
			begin
			if(hcount >= 105 && hcount < 135 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 125 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 125 && hcount < 135 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 135 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 115 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 115 && hcount < 135 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 105 && hcount < 135 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 135 && hcount < 140 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm >=30 && outm < 40)
			begin
			if(hcount >= 105 && hcount < 135 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 125 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 125 && hcount < 135 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 135 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 125 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 125 && hcount < 135 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 135 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 135 && hcount < 140 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm >=40 && outm < 50)
			begin
			if(hcount >= 105 && hcount < 115 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 115 && hcount < 125 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 125 && hcount < 135 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if (hcount >= 105 && hcount < 135 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;	
				else if(hcount >= 105 && hcount < 125 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;	
				else if(hcount >= 125 && hcount < 135 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 135 && hcount < 140 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm >=50 && outm < 60)
			begin
			if(hcount >= 105 && hcount < 135 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 115 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 115 && hcount < 135 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 105 && hcount < 135 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 125 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 125 && hcount < 135 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 105 && hcount < 135 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 135 && hcount < 140 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		//MINS 00	
		if(outm == 6'd0 || outm == 6'd10 || outm == 6'd20 || outm == 6'd30 || outm == 6'd40 || outm == 6'd50)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 30 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;	
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd1 || outm == 6'd11 || outm == 6'd21 || outm == 6'd31 || outm == 6'd41 || outm == 6'd51)
			begin
				if(hcount >= 140 && hcount < 160 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
				else if (hcount >= 160 && hcount < 170 && vcount  >= 20 && vcount < 70)
					rgb = 3'b001;
				else if (hcount >= 170 && hcount < 175 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd2 || outm == 6'd12 || outm == 6'd22 || outm == 6'd32 || outm == 6'd42 || outm == 6'd52)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 170 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd3 || outm == 6'd13 || outm == 6'd23 || outm == 6'd33 || outm == 6'd43 || outm == 6'd53)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd4 || outm == 6'd14 || outm == 6'd24 || outm == 6'd34 || outm == 6'd44 || outm == 6'd54)
			begin
				if(hcount >= 140 && hcount < 150 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if (hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;	
				else if(hcount >= 140 && hcount < 160 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;	
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd5 || outm == 6'd15 || outm == 6'd25 || outm == 6'd35 || outm == 6'd45 || outm == 6'd55)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 170 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd6 || outm == 6'd16 || outm == 6'd26 || outm == 6'd36 || outm == 6'd46 || outm == 6'd56)
			begin
				if(hcount >= 140 && hcount < 150 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 170 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end	
		else if(outm == 6'd7 || outm == 6'd17 || outm == 6'd27 || outm == 6'd37 || outm == 6'd47 || outm == 6'd57)
			begin
				if(hcount >= 140 && hcount < 170 && vcount > 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 30 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd8 || outm == 6'd18 || outm == 6'd28 || outm == 6'd38 || outm == 6'd48 || outm == 6'd58)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			
				else if(hcount >= 140 && hcount < 150 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;	
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outm == 6'd9 || outm == 6'd19 || outm == 6'd29 || outm == 6'd39 || outm == 6'd49 || outm == 6'd59)
			begin
				if(hcount >= 140 && hcount < 170 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 150 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 150 && hcount < 160 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 170 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 140 && hcount < 160 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 160 && hcount < 170 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 170 && hcount < 175 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
			//HR 01
		if(outh >=0 && outh < 10)
			begin
			if(hcount >= 20 && hcount < 50 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 20 && hcount < 30 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 30 && hcount < 40 && vcount >= 30 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 40 && hcount < 50 && vcount >= 30 && vcount < 60)
					rgb = 3'b001;	
				else if(hcount >= 20 && hcount < 50 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 50 && hcount < 55 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh >=10 && outh < 20)
			begin
				if(hcount >= 20 && hcount < 40 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
				else if (hcount >= 40 && hcount < 50 && vcount  >= 20 && vcount < 70)
					rgb = 3'b001;
				else if (hcount >= 50 && hcount < 55 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh >=20 && outh < 30)
			begin
				if(hcount >= 20 && hcount < 50 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 20 && hcount < 40 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 40 && hcount < 50 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 20 && hcount < 50 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 20 && hcount < 30 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 30 && hcount < 50 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 20 && hcount < 50 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 50 && hcount < 55 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
			//HR 00
		if(outh == 5'd0 || outh == 5'd10 || outh == 5'd20)
			begin
			if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			else if(hcount >= 55 && hcount < 65 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;
			else if(hcount >= 65 && hcount < 75 && vcount >= 30 && vcount < 60)
				rgb = 3'b000;
			else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 60)
				rgb = 3'b001;	
			else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
				rgb = 3'b001;
			else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
				rgb = 3'b000;
			end
		else if(outh == 5'd1 || outh == 5'd11 || outh == 5'd21)
			begin
				if(hcount >= 55 && hcount < 75 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
				else if (hcount >= 75 && hcount < 85 && vcount  >= 20 && vcount < 70)
					rgb = 3'b001;
				else if (hcount >= 85 && hcount < 90 && vcount  >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd2 || outh == 5'd12 || outh == 5'd22)
			begin
				if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 65 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 85 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd3 || outh == 5'd13 || outh == 5'd23)
			begin
				if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd4 || outh == 5'd14)
			begin
				if(hcount >= 55 && hcount < 65 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 75 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if (hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;	
				else if(hcount >= 55 && hcount < 75 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;	
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd5 || outh == 5'd15)
			begin
				if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 65 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 85 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd6 || outh == 5'd16)
			begin
				if(hcount >= 55 && hcount < 65 && vcount >= 20 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 85 && vcount >= 20 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 65 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 75 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
			
		else if(outh == 5'd7 || outh == 5'd17)
			begin
				if(hcount >= 55 && hcount < 85 && vcount > 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 30 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd8 || outh == 5'd18)
			begin
				if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
				rgb = 3'b001;
			
				else if(hcount >= 55 && hcount < 65 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 75 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;	
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 65 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 75 && vcount >= 50 && vcount < 60)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 60)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 60 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
		else if(outh == 5'd9 || outh == 5'd19)
			begin
				if(hcount >= 55 && hcount < 85 && vcount >= 20 && vcount < 30)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 65 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 65 && hcount < 75 && vcount >= 30 && vcount < 40)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 30 && vcount < 40)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 85 && vcount >= 40 && vcount < 50)
					rgb = 3'b001;
				else if(hcount >= 55 && hcount < 75 && vcount >= 50 && vcount < 70)
					rgb = 3'b000;
				else if(hcount >= 75 && hcount < 85 && vcount >= 50 && vcount < 70)
					rgb = 3'b001;
				else if(hcount >= 85 && hcount < 90 && vcount >= 20 && vcount < 70)
					rgb = 3'b000;
			end
			/*if(hcount >= 105 && hcount < 135 && vcount >= 90 && vcount < 100)
				rgb = 3'b001;
			else if(hcount >= 105 && hcount < 115 && vcount >= 100 && vcount < 130)
				rgb = 3'b001;
			else if(hcount >= 115 && hcount < 125 && vcount >= 100 && vcount < 130)
				rgb = 3'b000;
			else if(hcount >= 125 && hcount < 135 && vcount >= 100 && vcount < 130)
				rgb = 3'b001;	
			else if(hcount >= 105 && hcount < 135 && vcount >= 130 && vcount < 140)
				rgb = 3'b001;
			else if(hcount >= 135 && hcount < 140 && vcount >= 90 && vcount < 140)
				rgb = 3'b000;*/
			
			
			if(hcount >= 175 && hcount < 640 && vcount >= 20 && vcount < 70)
				rgb = 3'b000;
end
else
	rgb = 3'b000;
end
end
endmodule
